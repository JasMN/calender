import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import  {  NgxHijriGregorianDatepickerModule  }  from  'ngx-hijri-gregorian-datepicker';
import { JassCalenderComponent } from './jass-calender/jass-calender.component'
@NgModule({
  declarations: [
    AppComponent,
    JassCalenderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgxHijriGregorianDatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
