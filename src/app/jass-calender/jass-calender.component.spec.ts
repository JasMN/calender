import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JassCalenderComponent } from './jass-calender.component';

describe('JassCalenderComponent', () => {
  let component: JassCalenderComponent;
  let fixture: ComponentFixture<JassCalenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JassCalenderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JassCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
