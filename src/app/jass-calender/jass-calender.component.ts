import { Component, Input } from '@angular/core';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateType } from 'ngx-hijri-gregorian-datepicker';

@Component({
  selector: 'app-jass-calender',
  templateUrl: './jass-calender.component.html',
  styleUrls: ['./jass-calender.component.scss']
})
export class JassCalenderComponent {
  @Input() inputName:string = "التاريخ";
  today = new Date();
  selectedDate: NgbDateStruct
  selectedDateType  =  DateType.Hijri;
  constructor()  {
    const today = new Date();
    this.selectedDate = { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() };
  }
}
